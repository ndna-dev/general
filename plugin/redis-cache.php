<?php
if ( ! defined( 'ABSPATH' ) ) exit;

$plugins = get_option( 'active_plugins' );
if ( in_array( 'redis-cache/redis-cache.php' , $plugins ) ) {
    add_action('admin_init', 'disable_dashboard_redis_cache');
    add_action('admin_init', 'disable_redis_cache_disable_notice', 999);
}

/**
 * Remove dashboard widget(s)
 */
function disable_dashboard_redis_cache() {
    remove_meta_box( 'dashboard_rediscache', 'dashboard', 'normal' );
}

/**
 * Disable notice(s)
 */
function disable_redis_cache_disable_notice() {
    update_user_meta(get_current_user_id(), 'roc_dismissed_pro_release_notice', true);
}