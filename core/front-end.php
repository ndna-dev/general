<?php 
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Remove actions
 */
remove_action( 'wp_head', 'wp_generator' ); // Remove WordPress version
remove_action( 'wp_head', 'rsd_link' ); // Remove RSD Link
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'feed_links', 2 ); // Hide the non-essential WordPress RSS Feeds (1/2)
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Hide the non-essential WordPress RSS Feeds (2/2)
/**
 * Action - Disable Emoji (Smiley) and remove code
 */
if ( !function_exists('disable_emojis') ) {
	add_action( 'init', 'disable_emojis' );
	
	function disable_emojis() {
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	}
}