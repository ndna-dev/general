<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Configuration array
 */
$aConfig = array(
    // Company
    'company' => array(
        // Company - NDRK
        'ndrk' => array(
            'name' => 'NDRK Creatieve Communicatie',
            'short_name' => 'NDRK',
            'contact' => 'Peter Flikweert',
            'address' => 'De Weel 13',
            'zip_code' => '4306 NV',
            'city' => 'Nieuwerkerk',
            'phone_number' => '06 24 46 31 23',
            'emailaddress' => 'peter@ndrk.nl',
            'website' => 'http://www.ndrk.nl',
            'logo' => array(
                'src' => 'http://ndrk.nl/files/uploads/ndrk_zwart1.svg',
                'width' => 296,
                'height' => 40
            )
        ),
        // Company - de-zeeuw.nl
        'dezeeuw' => array(
            'name' => 'de-zeeuw.nl',
            'contact' => 'Joeri de Zeeuw',
            'address' => 'Dorpsweg 3',
            'zip_code' => '4311 AG',
            'city' => 'Bruinisse',
            'phone_number' => '',
            'emailaddress' => 'joeri@de-zeeuw.nl',
            'website' => 'http://www.de-zeeuw.nl',
            'logo' => array(
                'src' => get_template_directory_uri() .'/assets/images/logo/logo-de-zeeuw.jpg',
                'width' => 149,
                'height' => 149
            )
        ),
        // Company - Flikweert IT
        'flikweertit' => array(
            'name' => 'Flikweert IT',
            'contact' => 'Corn&eacute; Flikweert',
            'address' => 'Keersweel 3',
            'zip_code' => '4311 CS',
            'city' => 'Bruinisse',
            'phone_number' => '06 29 07 37 17',
            'emailaddress' => 'support@flikweert-it.nl',
            'website' => 'http://www.flikweert-it.nl',
            'logo' => array(
                'src' => get_template_directory_uri() .'/assets/images/logo/logo-flikweert-it.png',
                'width' => '180',
                'height' => '126',
            )
        ),
        // Company - Vivars
        'vivars' => array(
            'name' => 'Vollan',
            'contact' => '',
            'address' => 'Grevelingenstraat 10 H',
            'zip_code' => '4301 XX',
            'city' => 'Zierikzee',
            'phone_number' => '085 30 38 139',
            'emailaddress' => 'info@vollan.nl',
            'website' => 'https://www.vollan.nl',
            'logo' => array(
                'src' => get_template_directory_uri() .'/assets/images/logo/logo-vollan.svg',
                'width' => '180',
                'height' => '126',
            )
        ),
        // Company - TechFarm
        'techfarm' => array(
            'name' => 'Techfarm',
            'contact' => '',
            'address' => 'Grevelingenstraat 10 H',
            'zip_code' => '4301 XX',
            'city' => 'Zierikzee',
            'phone_number' => '085 30 38 139',
            'emailaddress' => 'grow@techfarm.nl',
            'website' => 'https://www.techfarm.nl',
            'logo' => array(
                //'src' => get_template_directory_uri() .'/assets/images/logo/logo-vivars.svg',
                'width' => '150',
                'height' => '108',
            )
        )
    )
);