<?php 
if ( ! defined( 'ABSPATH' ) ) exit;
// ####################################################################################################
// ####################################################################################################
// If option 'website_manage_htaccess' exists then continue
if( get_option( 'website_manage_htaccess' ) ){
	
/**
 * Add rules to .htaccess
 */
function extend_htaccess( $rules ){

$rule = <<<EOD
#php_flag opcache.enable Off

### Disable directory browsing ###
Options -Indexes

### Block WordPress xmlrpc.php requests ###
<files xmlrpc.php>
	Require all denied
	ErrorDocument 403 http://127.0.0.1/
</files>

### Protect WordPress configuration wp-config.php file from unauthorized access ###
<files wp-config.php>
	order allow,deny
	deny from all
</files>

### Protect .htaccess file from unauthorized access ###
<files ~ "^.*\.([Hh][Tt][Aa])">
	order allow,deny
	deny from all
	satisfy all
</files>\n\n
EOD;
    
    return $rule . $rules;
}
add_filter('mod_rewrite_rules', 'extend_htaccess');

// ####################################################################################################
// If option 'website_manage_htaccess_admin_ip' exists then continue
if( get_option( 'website_manage_htaccess_admin_ip' ) ){
/**
 * Add rules to .htaccess for WordPress admin access
 */
function extend_htaccess_wp_login( $rules ){
	$ip_table = array(
		array(
			'ip' => '178.22.63.37',
			'name' => '#Server - SRV1'
		),
		array(
			'ip' => '81.207.236.223',
			'name' => '#IP - Flikweert'
		),
		array(
			'ip' => '84.243.217.56',
			'name' => '#IP - Vivars'
		),
	);
	
$rule = <<<EOD
\n### Protect wp-login | Allow specific IP's ###
<files wp-login.php>
	ErrorDocument 403 http://127.0.0.1/
	order deny,allow\n
EOD;

foreach($ip_table as $ip){
	$address = $ip['ip'];
	$name = $ip['name'];
	
$rule .= <<<EOD
	allow from $address $name\n
EOD;
}

$rule .= <<<EOD
	deny from all
</files>\n\n
EOD;
    
    return $rule . $rules;
}
add_filter('mod_rewrite_rules', 'extend_htaccess_wp_login');
}
// ####################################################################################################

}
// ####################################################################################################
// ####################################################################################################