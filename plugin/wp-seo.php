<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * This function checks whether the WordPress SEO plugin is active (it needs to be active for SO Clean Up WP SEO to have any use)
 * if the WordPress SEO plugin has been installed, add the actions and filters that clean up the entire WP SEO experience
 */
$plugins = get_option( 'active_plugins' );
if ( in_array( 'wordpress-seo/wp-seo.php' , $plugins ) ) {
	add_action('admin_head', 'disable_wp_seo_sidebar_ads');
	add_action('admin_init', 'disable_wp_seo_disable_notice', 999);
	add_action('admin_init', 'disable_wp_seo_dashboard_widgets');
	add_action('admin_bar_menu', 'remove_wp_seo_menu_bar', 999);
}

/**
 * Remove dashboard widget(s)
 */
function disable_wp_seo_dashboard_widgets() {
	// Yoast SEO
	remove_meta_box( 'yoast_db_widget', 'dashboard', 'normal' );
	remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal' );
}
/**
 * WP SEO - Remove ads and notices
 */
function disable_wp_seo_sidebar_ads() {
	echo '<style type="text/css">
	#wpseo-dismiss-about, 
	#sidebar-container.wpseo_content_cell, 
	.wpseotab.active > p:nth-child(6), 
	.wpseotab.active > p:nth-child(7),
	#i18n_promo_box	{display:none;}
	</style>';
}
/**
 * WP SEO - Disable notice(s)
 */
function disable_wp_seo_disable_notice() {	
	update_user_meta(get_current_user_id(), 'wpseo_ignore_tour', true);
	update_user_meta(get_current_user_id(), 'wpseo_dismissed_gsc_notice', true);
}
/**
 * WP SEO - Remove item from menu bar
 */
function remove_wp_seo_menu_bar( $wp_admin_bar ) {
	$wp_admin_bar->remove_menu( 'wpseo-menu' );
}

/**
 * Convert Yoast breadcrumbs to theme specific style
 *
 * @params string $breadcrumbs Breadcrumb HTML
 * @return string
 **/

function convert_wp_seo_breadcrumbs($breadcrumbs){
	$breadcrumbs = preg_replace(
		'/<span><a href="([^"]+)" >([^<]+)<\\/a>/',
		'<li><a href="$1">$2</a></li>'."\r\n",
		$breadcrumbs
	);

	$breadcrumbs = preg_replace(
		'/<span class="breadcrumb_last">([^<]+)<\\/span>/',
		"\t\t\t\t\t".'<li>$1</li>',
		$breadcrumbs
	);
	
	$breadcrumbs = preg_replace(
		'/<\\/span>/','',
		$breadcrumbs
	);

	$breadcrumbs = preg_replace(
		'/<span>/','',
		$breadcrumbs
	);

	return $breadcrumbs;
}

if ( has_filter( 'wpseo_breadcrumb_output', 'convertBreadcrumbs' ) ){
	add_filter('wpseo_breadcrumb_output', 'convert_wp_seo_breadcrumbs', 20, 1);
}