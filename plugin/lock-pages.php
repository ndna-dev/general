<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * This function checks whether the WordPress Lock Pages plugin is active
 */
$plugins = get_option( 'active_plugins' );
if ( in_array( 'Lock-Pages-master/lock-pages.php' , $plugins ) ) {
	add_action('admin_head', 'disable_lock_pages_notice');
}

/**
 * Remove notices
 */
function disable_lock_pages_notice() {
	echo '<style type="text/css">
	.page-locked-notice {
		display: none;
	}
	</style>';
}