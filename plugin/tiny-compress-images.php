<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

$plugins = get_option( 'active_plugins' );
if ( in_array( 'tiny-compress-images/tiny-compress-images.php' , $plugins ) ) {
	add_action('admin_init', 'disable_tiny_compress_images_dashboard_widgets');
}

/**
 * Remove dashboard widget(s)
 */
function disable_tiny_compress_images_dashboard_widgets() {
	remove_meta_box( 'tinypng_dashboard_widget', 'dashboard', 'normal' );
}