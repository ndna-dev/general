<?php 
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Change the link on the login logo from WordPress.org to the site's homepage
 *
 * @uses	home_url()
 * @return string	The site's homepage URL
 */
add_filter( 'login_headerurl', 'custom_login_headerurl' );
function custom_login_headerurl() {
	return home_url();
}
/**
 * Change the title on the login logo to the site name
 *
 * @uses	get_option()
 * @return string	The site name
 */
add_filter( 'login_headertext', 'custom_login_headertext' );
function custom_login_headertext() {
    return get_option( 'blogname' );
}