<?php
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Convert Yoast breadcrumbs to theme specific style
 *
 * @param $breadcrumbs
 * @return mixed|string|string[]|null
 * @author Vivars Internet
 */
add_filter('wpseo_breadcrumb_output', 'breadcrumb_li', 11, 1);
function breadcrumb_li($breadcrumbs)
{
    $breadcrumbs = preg_replace(
        '/<a href="([^"]+)">([^<]+)<\\/a>/',
        '<li><a href="$1">$2</a></li>' . "\r\n",
        $breadcrumbs
    );

    $breadcrumbs = preg_replace(
        '/<span class="breadcrumb_last" aria-current="page">([^<]+)<\\/span>/',
        "\t\t\t\t\t".'<li>$1</li>',
        $breadcrumbs
    );

    return $breadcrumbs;
}
