<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * This function checks whether the WordPress Lock Pages plugin is active
 */
$plugins = get_option( 'active_plugins' );
if ( in_array( 'gravityforms/gravityforms.php' , $plugins ) ) {
    // Empty legend
    add_filter( 'gform_required_legend', '__return_empty_string' );
}