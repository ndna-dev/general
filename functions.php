<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Update checker
 * https://github.com/YahnisElsts/plugin-update-checker
 */
$vivUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/ndna-dev/general',
    __FILE__, //Full path to the main plugin file or functions.php.
    'general'
);

$vivUpdateChecker->setBranch('master');

/**
 * ====================
 * Include/Require
 * ====================
 */
// Load configuration file
require_once( 'include/config.array.php' );

// Check if class 'Includer' is already declared
if (!class_exists('Includer')) {
	require_once( 'class/includer.class.php' );
}
// Include
new Includer( get_template_directory() . '/core/' );
new Includer( get_template_directory() . '/plugin/' );

/**
 * ====================
 * Set default values for display settings inside the upload media box
 * ====================
 */
add_action('after_setup_theme', 'update_media_settings');
function update_media_settings() {
	update_option('image_default_align', 'center' );
	update_option('image_default_link_type', 'media' );
	update_option('image_default_size', 'large' );
}

/**
 * ====================
 * Load CSS for WP-admin login
 * ====================
 */
add_action( 'login_enqueue_scripts', 'custom_login_enqueue_scripts' );
function custom_login_enqueue_scripts() {
	wp_enqueue_style( 'admin-login', get_template_directory_uri() .'/assets/css/admin-login.css' );
	if(defined('COMPANY'))
		wp_enqueue_style( 'admin-login-company', get_template_directory_uri() .'/assets/css/admin-login-'. COMPANY .'.css' );
}
/**
 * ====================
 * Load CSS for WP-admin
 * ====================
 */
add_action( 'admin_enqueue_scripts', 'custom_admin_enqueue_scripts' );
function custom_admin_enqueue_scripts() {
	wp_enqueue_style( 'admin', get_template_directory_uri() .'/assets/css/admin-widget.css' );
}

/**
 * ====================
 * Load Cookiebar css
 * ====================
 */
function custom_cookiebar_style() {
    wp_dequeue_style( 'cookie-notice-front');
    wp_enqueue_style( 'continually-fix', get_template_directory_uri() .'/assets/css/continually.css', [], 202111101130);
    wp_enqueue_style( 'default-cookie', get_template_directory_uri() .'/assets/css/cookie.css', [], 202111101130);
    wp_enqueue_script( 'default-cookie', get_template_directory_uri() .'/assets/js/cookie.js', [], 202111101130);
}

if (defined('CUSTOM_COOKIENOTICE') && CUSTOM_COOKIENOTICE) {
    add_action( 'wp_enqueue_scripts', 'custom_cookiebar_style', 25);
}
