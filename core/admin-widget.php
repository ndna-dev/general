<?php 
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * WP Admin - Disable dashboard widgets
 * Disable some of the dashboard widgets that are included with WordPress Core and somepopular plugins.
 *
 * Remove or comment-out lines as appropriate
 *
 * @uses   remove_meta_box()
 * @return void
 */
add_action('admin_init', 'disable_default_dashboard_widgets');
function disable_default_dashboard_widgets() {
	// WordPress blog
	remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );
	// Activity
	remove_meta_box( 'dashboard_activity', 'dashboard', 'core' );
	// Incoming links
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' );
	// Plugins
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );
	// Other WordPress news
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'core' );
	// WPML
	remove_meta_box( 'icl_dashboard_widget', 'dashboard', 'normal' );
    // Site health
    remove_meta_box( 'dashboard_site_health', 'dashboard', 'normal' );
	
	if(defined('WEBSITE_TYPE')){
		if(WEBSITE_TYPE == 'website'){
			// Right Now
			remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' );
			// Comments
			remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'core' );
			// QuickPress
			remove_meta_box( 'dashboard_quick_press', 'dashboard', 'core' );
			// Recent drafts
			remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'core' );
		}
	}
}
/**
 * WP Admin - Add custom widgets
 */
function dashboard_widget_release() {
	global $aConfig;
	if(!empty($aConfig['company'][COMPANY]['logo']['src']))
		echo '<img src="'. $aConfig['company'][COMPANY]['logo']['src'] .'" class="company_logo" width="'. $aConfig['company'][COMPANY]['logo']['width'] .'" height="'. $aConfig['company'][COMPANY]['logo']['height'] .'" alt="'. $aConfig['company'][COMPANY]['name'] .'" title="'. $aConfig['company'][COMPANY]['name'] .'" />'.PHP_EOL;
	echo '<table class="company_information">'.PHP_EOL;
		echo '<tr>'.PHP_EOL;
			echo '<td>Contactpersoon</td>'.PHP_EOL;
			echo '<td>'. $aConfig['company'][COMPANY]['contact'] .'</td>'.PHP_EOL;
		echo '</tr>'.PHP_EOL;
		echo '<tr>'.PHP_EOL;
			echo '<td>Adres</td>'.PHP_EOL;
			echo '<td>'. $aConfig['company'][COMPANY]['address'] .'</td>'.PHP_EOL;
		echo '</tr>'.PHP_EOL;
		echo '<tr>'.PHP_EOL;
			echo '<td>&nbsp;</td>'.PHP_EOL;
			echo '<td>'. $aConfig['company'][COMPANY]['zip_code'] .' '. $aConfig['company'][COMPANY]['city'] .'</td>'.PHP_EOL;
		echo '</tr>'.PHP_EOL;
		if(!empty($aConfig['company'][COMPANY]['phone_number'])){
			echo '<tr>'.PHP_EOL;
				echo '<td>Telefoonnummer</td>'.PHP_EOL;
				echo '<td><a href="tel:'. $aConfig['company'][COMPANY]['phone_number'] .'">'. $aConfig['company'][COMPANY]['phone_number'] .'</a></td>'.PHP_EOL;
			echo '</tr>'.PHP_EOL;
		}
		echo '<tr>'.PHP_EOL;
			echo '<td>E-mail</td>'.PHP_EOL;
			echo '<td><a href="mailto:'. $aConfig['company'][COMPANY]['emailaddress'] .'">'. $aConfig['company'][COMPANY]['emailaddress'] .'</a></td>'.PHP_EOL;
		echo '</tr>'.PHP_EOL;
		echo '<tr>'.PHP_EOL;
			echo '<td>Website</td>'.PHP_EOL;
			echo '<td><a href="'. $aConfig['company'][COMPANY]['website'] .'" target="_blank">'. $aConfig['company'][COMPANY]['website'] .'</a></td>'.PHP_EOL;
		echo '</tr>'.PHP_EOL;
	echo '</table>'.PHP_EOL;
}
add_action('wp_dashboard_setup', 'wp_add_custom_dashboard_widget' );
function wp_add_custom_dashboard_widget() {
	if(defined('COMPANY')){
		wp_add_dashboard_widget('wp_company_dashboard_widget', 'Realisatie', 'dashboard_widget_release');
	}
}
