<?php if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Function admin_panel_menu_item
 */
function admin_panel_menu_item(){
	add_menu_page(
		'Beheerder instellingen', 
		'Beheerder instellingen', 
		'edit_theme_options', 
		'admin-panel', 
		'admin_panel_settings_page', 
		null, 
		9999
	);
}
add_action('admin_menu', 'admin_panel_menu_item');

/**
 * Function admin_panel_settings_page
 */
function admin_panel_settings_page(){
    ?>
    <div class="wrap">
	    <h1>Beheerder instellingen <?php echo bloginfo('name'); ?></h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
	</div>
	<?php
}

/**
 * Function display_website_api_key_element
 */
function field_website_api_key(){
	echo '<input type="text" name="website_api_key" id="website_api_key" value="'. get_option('website_api_key') .'" size="60" />';
}

/**
 * Function display_website_manage_htaccess
 */
function field_website_manage_htaccess(){
	$option = get_option('website_manage_htaccess');
	echo '<input type="checkbox" name="website_manage_htaccess" id="website_manage_htaccess" value="1" '. checked( 1, $option, false ) .'>';
}

/**
 * Function display_website_manage_htaccess_admin_ip
 */
function field_website_manage_htaccess_admin_ip(){
	$option = get_option('website_manage_htaccess_admin_ip');
	echo '<input type="checkbox" name="website_manage_htaccess_admin_ip" id="website_manage_htaccess_admin_ip" value="1" '. checked( 1, $option, false ) .'>';
}

/**
 * Function display_theme_panel_fields
 */
function display_theme_panel_fields(){
	// Add section	
	add_settings_section('section', 'API', null, 'theme-options');
	// Add field
	add_settings_field('website_api_key', 'API-sleutel', 'field_website_api_key', 'theme-options', 'section');
  	// Register field
    register_setting('section', 'website_api_key');
	// Add field
	add_settings_field('website_manage_htaccess', '.htaccess beheer inschakelen?', 'field_website_manage_htaccess', 'theme-options', 'section');
  	// Register field
    register_setting('section', 'website_manage_htaccess');
	// Add field
	add_settings_field('website_manage_htaccess_admin_ip', '.htaccess Admin IP-adressen beheer inschakelen?', 'field_website_manage_htaccess_admin_ip', 'theme-options', 'section');
  	// Register field
    register_setting('section', 'website_manage_htaccess_admin_ip');
}

add_action('admin_init', 'display_theme_panel_fields');

/**
 * Check - API
 * Check if the API key is entered. If not, an error message is displayed.
 */
if( !get_option( 'website_api_key' ) ){
	add_action( 'admin_notices', 'admin_notice_api_key_error' );
}

function admin_notice_api_key_error() {
	$class = 'notice notice-error is-dismissible';
	$message = 'De API-sleutel van deze WordPress installatie is niet of onjuist ingesteld. Klik <a href="'. admin_url( 'admin.php?page=admin-panel' )
	 . '">hier</a> om de instellingen aan te passen.';

	printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 
}