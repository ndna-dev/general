var cookie = function () {
    var cookieNotice = document.querySelector('#cookie-notice');
    var title = cookieNotice.querySelector('h4');

    var textContainer = document.getElementById('cn-notice-text');
    var revokeButton = document.getElementById('cn-refuse-cookie');
    var acceptButton = document.getElementById('cn-accept-cookie');
    var moreInfoButton = document.getElementById('cn-more-info');

    if (textContainer && title) {
        textContainer.prepend(title);
    }

    if (textContainer && moreInfoButton) {
        moreInfoButton.className = '';
        textContainer.append(' ');
        textContainer.append(moreInfoButton);
    }

    if (acceptButton) {
        acceptButton.classList.add('btn');
    }

    if (revokeButton) {
        revokeButton.classList.add('btn');
    }
};

document.addEventListener('DOMContentLoaded', cookie);