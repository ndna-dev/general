<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

add_action('admin_head', 'custom_css_advanced_custom_fields_pro');

/**
 * Add custom CSS
 */
function custom_css_advanced_custom_fields_pro() {
	echo '<style type="text/css">
	.acf-flexible-content .layout .acf-fc-layout-handle {
		border-bottom: #23282d solid 1px;
		background-color: #23282d;
		color: #fff;
	}

	.acf-flexible-content .layout .acf-fc-layout-controlls .acf-icon.-collapse:hover {
		background-color: #fff;
	}
	</style>';
}