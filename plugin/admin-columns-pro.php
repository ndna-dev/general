<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

$plugins = get_option( 'active_plugins' );
if ( in_array( 'admin-columns-pro/admin-columns-pro.php' , $plugins ) ) {
	add_action('admin_head', 'custom_css_admin_columns_pro');
}

/**
 * Add custom CSS
 */
function custom_css_admin_columns_pro() {
	echo '<style type="text/css">
	/* Delete plugin support block */
	.ac-right #plugin-support {
		display: none;
	}
	</style>';
}