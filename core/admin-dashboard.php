<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Update user meta to hide 'Try Gutenberg panel'
 */
add_action('admin_init', 'disable_try_gutenberg_panel');
function disable_try_gutenberg_panel() {
	global $wp_version;
	if ( version_compare( $wp_version, '4.9.8', '>=' ) ) {
		$user_id = get_current_user_id();

		if( !( get_user_meta($user_id, 'show_try_gutenberg_panel', true) ) ){
			update_user_meta($user_id, 'show_try_gutenberg_panel', 0 );
		}elseif( get_user_meta($user_id, 'show_try_gutenberg_panel', true) == 1){
			update_user_meta($user_id, 'show_try_gutenberg_panel', 0 );
		}
	}
}

/**
 * Add a personal message in the admin footer.
 *
 * @uses   wp_get_theme()  To get data from the active theme
 * @param  string $content The original admin footer
 * @return string          The modified admin footer
 */
if(defined('COMPANY'))
	add_filter( 'admin_footer_text', 'custom_admin_footer' );
function custom_admin_footer( $content ) {
	global $aConfig;
	return sprintf (
		__( ' Realisatie: <a target="_blank" href="%2$s">%1s</a>'),
		$aConfig['company'][COMPANY]['name'],
		$aConfig['company'][COMPANY]['website']
	);
}

// WP Admin - Disable welcome panel
remove_action( 'welcome_panel', 'wp_welcome_panel' );

/**
 * Remove 'Help' tab
 */
add_action('admin_head', 'current_screen_remove_contextual_help');
function current_screen_remove_contextual_help()
{
    $screen = get_current_screen();
    $screen->remove_help_tabs();
}

/**
 * Change default contact methods
 */
add_filter( 'user_contactmethods', 'change_profile_fields', 10, 1 );
function change_profile_fields( $contactmethods ) {	
	unset( $contactmethods['facebook'] );
	unset( $contactmethods['twitter'] );
	unset( $contactmethods['googleplus'] );
	
	return $contactmethods;
}

/**
 * Remove menus from the admin bar
 *
 * @uses   $wp_admin_bar
 * @return void
 */
add_action( 'wp_before_admin_bar_render', 'custom_admin_bar' );
add_filter( 'show_admin_bar', '__return_false' ); // Disable WP Admin bar
function custom_admin_bar() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'wp-logo' ); // WordPress logo
	$wp_admin_bar->remove_menu( 'comments' ); // Comment
	$wp_admin_bar->remove_menu( 'search' ); // Search
	$wp_admin_bar->remove_menu( 'appearance' ); // Thema's / Customizer
}

/**
 * Hide admin menu items
 *
 * @uses   current_user_can() To check if the logged in user is an administrator
 * @uses   remove_menu_page()
 * @return void
 */
add_action( 'admin_menu', 'remove_menus' );
function remove_menus() {
	/* Check if the logged in user is an administrator */
	if ( ! current_user_can( 'manage_options' ) ) {
		remove_menu_page( 'jetpack' ); // Jetpack
		remove_menu_page( 'options-general.php' ); // Settings
		remove_menu_page( 'tools.php' ); // Tools
	}
}