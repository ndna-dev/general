<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Function: send_login_notification
 * Log a user login to a remote API.  
 */
function log_user_login($user_login, $user) {
	// If option 'website_api_key' exists then continue with the cURL request
	if( get_option( 'website_api_key' ) ){
			
		// cURL - HTTP Header(s)	
		$http_headers = array(
			'api-key:'. get_option( 'website_api_key' ) //API Key
		);
		
		// cURL - Post field(s)
		$post_fields = array(
	        'api_key' => get_option( 'website_api_key' ),
	        'user' => $user_login,
	        'ip_address' => $_SERVER['REMOTE_ADDR'],
	        'browser' => $_SERVER['HTTP_USER_AGENT']
	    );	
			    
		$curl_handle = curl_init();
	    curl_setopt_array($curl_handle, array(
	    	CURLOPT_URL => 'http://api.vivars.nl/api/wordpress/login',
	     	CURLOPT_RETURNTRANSFER => 1,
	    	CURLOPT_HTTPHEADER => $http_headers,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $post_fields
	    ));
		
	    $curl_result = curl_exec($curl_handle);
	    curl_close($curl_handle);
	     
	    $result = json_decode($curl_result);
	    if( isset($result->status) && $result->status == 'success'){
	    	$error = false;
		}else{
	    	$error = true;
			if( isset($result->status) ){
				$error_status = $result->status;
				$error_detail = $result->error;
			}else{
				$error_detail = 'Er kon geen verbinding worden gemaakt met de API';	
			}
		}
	// Else set $error to TRUE
	}else{
		$error = true;
		$error_status = 'Fout API-sleutel';
		$error_detail = 'WordPress \'website_api_key\' is niet ingesteld';
	}
	
	/**
	 * When $error is true, send a email
	 */
	if( $error ){
		// Set email data
		$mail_to = 'backup@vivars.email';
		$mail_subject = 'WordPress Login API Error - '. get_bloginfo('name') .' ('. get_bloginfo('url') .')';
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		
		$body = '<h1>Foutenrapport</h1>';
		$body .= '<p><strong>Website</strong>: '. get_bloginfo('name') .'<br/>';
		$body .= '<strong>URL</strong>: '. get_bloginfo('url') .'<br/>';
		$body .= '<strong>Gebruiker</strong>: '. $user_login .'</p>';
		
		$body .= '<p><strong>Status</strong>: '. $error_status .'<br/>';
		$body .= '<strong>Fout detail(s)</strong>: '. $error_detail .'</p>';
		
		$body .= '<p><strong>Datum</strong>: '. date('Y-m-d') .'<br/>';
		$body .= '<strong>Tijd</strong>: '. date('H:i:s') .'<p>';

		$body .= '<p><strong>IP-adres:</strong>: '. $_SERVER['REMOTE_ADDR'] .'<br/>';
		$body .= '<strong>Server IP-adres</strong>: '.  $_SERVER['SERVER_ADDR'] .'<br/>';
		
		// Send email
	    wp_mail($mail_to, $mail_subject, $body, $headers);
	}
}
add_action('wp_login', 'log_user_login', 10, 2);