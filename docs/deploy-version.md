## CI will publish the composer package when pushing a new version tag
+ Create version tag `git tag <VERSION>`
+ Commit changes `git commit -a -m "Release <VERSION>"`
+ Push changes with tags `git push --tags`
