<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * This function checks whether the WordPress Lock Pages plugin is active
 */
$plugins = get_option( 'active_plugins' );
if ( in_array( 'contact-form-7/wp-contact-form-7.php' , $plugins ) ) {
	add_action('admin_head', 'disable_wpcf7_notice');
}

/**
 * Remove notices
 */
function disable_wpcf7_notice() {
	echo '<style type="text/css">
	.toplevel_page_wpcf7 #welcome-panel {
		display: none;
	}
	</style>';
}